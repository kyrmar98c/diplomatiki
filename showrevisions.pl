#!/usr/bin/env perl
use strict;
use warnings;

my @FILES;

my $rpath = '.r';

my ($mainfile,$REVA) = @ARGV;
my $dpath = ".diff-old-new";

open(my $pipe, '-|', "svn diff --summarize -r {${REVA}}") or die;
while (<$pipe>) {
    next if not /^M.{7}(.*\.tex)$/;
    push @FILES, $1;
}
push @FILES, $mainfile;
close ($pipe);

exit (1) if not @FILES;

mkdir $rpath . 'revold';
mkdir $rpath . 'new';
mkdir $dpath;

foreach my $file (@FILES) {
    print $file, "\n";
    $file =~ /^(.*)\//;
    my $dir  = $1 || "";
    my $dira = $rpath . 'revold' . '/' . $dir;
    my $dirb = $rpath . 'new' . '/' . $dir;
    my $ddir = $dpath         . '/' . $dir;
    mkdir $dira if not -e $dira;
    mkdir $dirb if not -e $dirb;
    mkdir $ddir if not -e $ddir;
    system("svn cat -r {${REVA}} '$file' > '${rpath}revold/$file'");
    system("cp '$file' '${rpath}new/$file'");
    system("latexdiff '${rpath}revold/$file' '${rpath}new/$file' > '${dpath}/$file'");
}
