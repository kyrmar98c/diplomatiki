\contentsline {chapter}{Περίληψη}{iii}{chapter*.1}%
\contentsline {chapter}{Κατάλογος πινάκων}{x}{chapter*.3}%
\contentsline {chapter}{Κατάλογος σχημάτων}{xi}{chapter*.4}%
\contentsline {chapter}{Ευχαριστίες}{xii}{chapter*.5}%
\contentsline {chapter}{\numberline {1}Εισαγωγή}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Θεωρία}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Γενικά}{2}{section.2.1}%
\contentsline {section}{\numberline {2.2}Transformer}{3}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Attention is all you need}{3}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Swin Ιεραρχικός Transformer με χρήση shifted windows}{5}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Attentional συγχώνευση χαρακτηριστικών}{6}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}BotNet Αρχιτεκτονική}{6}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}CvT Αρχιτεκτονική}{7}{subsection.2.2.5}%
\contentsline {section}{\numberline {2.3}Αναγνώριση προσώπου}{9}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Φίλτρα Gabor}{9}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Αλγόριθμος Viola-Jones}{10}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Κατασκευαστικά αυτοσυσχετιζόμενα νευρωνικά δίκτυα}{11}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Πυραμιδωτά νευρωνικά δίκτυα}{14}{subsection.2.3.4}%
\contentsline {section}{\numberline {2.4}Νευρωνικά Δίκτυα}{17}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Αυτόματη αναγνώριση συναισθήματος}{17}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Αυτόματη αναγνώριση από την \gls {facial expression} }{17}{subsection.2.4.2}%
\contentsline {section}{\numberline {2.5}Συνελλικτικά Δίκτυα}{18}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Συνελλικτικό Δίκτυο δύο επιπέδων}{18}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Υβριδικό συνελλικτικό δίκτυο με SIFT αθροιστή }{19}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Συνελλικτικά Δικτυα και υπολογισμός αιχμών εικόνας}{21}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Αυτοκωδικοποιητές}{23}{subsection.2.5.4}%
\contentsline {subsection}{\numberline {2.5.5}Πολλαπλά στιγμιότυπα εικόνων από βίντεο και \gls {graph}}{23}{subsection.2.5.5}%
\contentsline {section}{\numberline {2.6}Εξαγωγή χαρακτηριστικών}{24}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Εξαγωγή χαρακτηριστικών και \gls {expression recognition}}{24}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}Εξαγωγή γεωμετρικών χαρακτηριστικών}{25}{subsection.2.6.2}%
\contentsline {subsection}{\numberline {2.6.3}Αναγνώριση προσώπου και συναισθήματος με χρήση Markov τυχαίων πεδίων}{26}{subsection.2.6.3}%
\contentsline {section}{\numberline {2.7}Μονάδες δράσης}{27}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Μονάδες δράσης και ορισμένη συναισθηματική έκφραση}{27}{subsection.2.7.1}%
\contentsline {subsection}{\numberline {2.7.2}Μονάδες δράσης και νευρωνικά δίκτυα}{28}{subsection.2.7.2}%
\contentsline {subsection}{\numberline {2.7.3}Μονάδες δράσης και εξειδικευμένα βαθιά νευρωνικά δίκτυα}{30}{subsection.2.7.3}%
\contentsline {chapter}{\numberline {3}Μέθοδοι}{31}{chapter.3}%
\contentsline {section}{\numberline {3.1}Οπισθοδιάδοση}{31}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Εισαγωγή}{31}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Αρχικοποίηση βαρών}{32}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Πρόβλεψη τιμών και υπολογισμός σφάλματος}{33}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Διόρθωση βαρών}{33}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Τερματισμός εκπαίδευσης}{36}{subsection.3.1.5}%
\contentsline {section}{\numberline {3.2}Μέθοδος Πρόβλεψης Συναισθήματος}{37}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Προεπεξεργασία εικόνων}{37}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Εκπαίδευση μοντέλου}{37}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Πρόβλεψη}{38}{subsection.3.2.3}%
\contentsline {chapter}{\numberline {4}Πειράματα}{39}{chapter.4}%
\contentsline {section}{\numberline {4.1}Οργάνωση πειραμάτων}{39}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Δεδομένα}{39}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Πείραμα 1- Custom νευρωνικό δίκτυο με 7 συναισθήματα}{40}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Πείραμα 2- Custom νευρωνικό δίκτυο με 4 συναισθήματα}{40}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Πείραμα 3- Custom νευρωνικό δίκτυο με 7 συναισθήματα και επιλυτές}{41}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}Πείραμα 4- Custom νευρωνικό δίκτυο με 7 συναισθήματα επιτροπή}{41}{subsection.4.1.5}%
\contentsline {subsection}{\numberline {4.1.6}Πείραμα 5- Χρήση Flux για νευρωνικό δίκτυο με 7 συναισθήματα}{42}{subsection.4.1.6}%
\contentsline {subsection}{\numberline {4.1.7}Πείραμα 6- Χρήση Flux για νευρωνικό δίκτυο με 4 συναισθήματα}{42}{subsection.4.1.7}%
\contentsline {subsection}{\numberline {4.1.8}Πείραμα 7- Χρήση Flux για νευρωνικό δίκτυο με 7 συναισθήματα και επιλυτές}{42}{subsection.4.1.8}%
\contentsline {subsection}{\numberline {4.1.9}Πείραμα 8- Χρήση Flux για νευρωνικό δίκτυο με 7 συναισθήματα επιτροπή}{42}{subsection.4.1.9}%
\contentsline {section}{\numberline {4.2}Χαρακτηριστικά Υπολογιστή}{43}{section.4.2}%
\contentsline {section}{\numberline {4.3}Αποτελέσματα Πειραμάτων}{43}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Πείραμα 1}{43}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Πείραμα 2}{43}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Πείραμα 3}{44}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}Πείραμε 4}{44}{subsection.4.3.4}%
\contentsline {subsection}{\numberline {4.3.5}Πείραμε 5}{45}{subsection.4.3.5}%
\contentsline {subsection}{\numberline {4.3.6}Πείραμε 6}{45}{subsection.4.3.6}%
\contentsline {subsection}{\numberline {4.3.7}Πείραμε 7}{46}{subsection.4.3.7}%
\contentsline {subsection}{\numberline {4.3.8}Πείραμε 8}{46}{subsection.4.3.8}%
\contentsline {chapter}{\numberline {5}Συμπεράσματα}{47}{chapter.5}%
\contentsline {section}{\numberline {5.1}Συζήτηση}{47}{section.5.1}%
\contentsline {section}{\numberline {5.2}Συμπερασμάτα}{48}{section.5.2}%
\contentsline {section}{\numberline {5.3}Μελοντικές επεκτάσεις}{49}{section.5.3}%
\contentsline {chapter}{\numberline {\MakeUppercase []{α\anw@true \anw@print \relax }}Πρώτο παράρτημα}{50}{appendix.Alph1}%
\contentsline {chapter}{\numberline {\MakeUppercase []{β\anw@true \anw@print \relax }}Δεύτερο παράρτημα}{51}{appendix.Alph2}%
\contentsline {section}{References}{52}{section*.24}%
\contentsline {chapter}{Βιβλιογραφία}{52}{section*.25}%
