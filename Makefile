#
#  Makefile 
#


### SHELL SYNTAX

SHELL := /bin/bash


### TARGETS

T_DOC = last-first-ProjACRON


### COMMANDS

TMARK   = ^^^
LATEX   = xelatex --synctex=1 -file-line-error
BIBTEX  = bibtex
LATEXMK = latexmk -pdf -pdflatex='xelatex %O -interaction=nonstopmode -synctex=1 $(TMARK)'
RM      = rm -rf
SVNUP   = svn update
CD      = cd
MAKE    = make
ECHO    = printf
SHOWRN  = texloganalyser -ahouvw
MAKEGLOSSARIES = makeglossaries


SHOWREV = svn diff
DIFF    = diff -bur
LTXDIFF = latexdiff --flatten --math-markup=0

REVISION = `svn info | grep Revision | cut -f2 -d: | tr -d '[:space:]'`

DEP = \def\depend{true}


# LaTeX compilation qualifiers
REV     = \newcommand{\revisionNumber}{$(REVISION)}



### CLEAN-UP EXTENSIONS

EXT_CLEAN = .tex~ .pdfsync \
	    .out 	    .dvi \
	    .pdf 	    .aux \
	    .toc 	    .lof \
	    .lot 	    .log \
	    .bbl 	    .blg \
	    .brf 	    .nav \
	    .snm        .spl \
	    .glg 	    .glo \
	    .gls 	    .ist \
	    .idx 	    .ilg \
        .fls        .fdb_latexmk \
	    .synctex.gz .run.xml \
	    -blx.bib \



### COMPILATION / MAKE

# PHONY targets are always compiled
.PHONY: draft nonotes update help usage rules


# default "make" target
.DEFAULT_GOAL := help

# -------------------- GLOBAL

all: draft			## (draft)
fast: fastdraft			## (fastdraft)


# -------------------- Draft

# "regular"
draft: clean			## Draft mode
	$(LATEX)  "$(REV) \input{$(T_DOC)}"
	$(BIBTEX) $(T_DOC)
	$(LATEX)  "$(REV) \input{$(T_DOC)}"
	$(MAKEGLOSSARIES) $(T_DOC)
	$(LATEX)  "$(REV) \input{$(T_DOC)}"

# "fast"
fastdraft:			## Fast draft mode
	$(subst $(TMARK),"$(REV) \input{$(T_DOC)}",$(LATEXMK)) $(T_DOC)

interactive:			## Interactively build PDF when changing sources
	$(subst $(TMARK),"$(REV) \input{$(T_DOC)}",$(LATEXMK)) -pvc $(T_DOC)

# -------------------- Revisions

revisions:		       ## Show revisions from previous version
	perl showrevisions.pl "$(T_DOC).tex" "'${DATE}'"
	rm -rf  /tmp/revisions
	mkdir -p /tmp/revisions/tex/article/
	cp -r . /tmp/revisions/tex/article/
	-mv .diff-old-new/sections/*.tex /tmp/revisions/tex/article/sections/
	-mv .diff-old-new/$(T_DOC).tex /tmp/revisions/tex/article/

	$(MAKE) -C /tmp/revisions/tex/article all

	mv /tmp/revisions//tex/article/$(T_DOC).pdf $(T_DOC)-revisions.pdf

	rm -rf /tmp/revisions
	rm -f $(T_DOC)-diff.*
	rm -rf .r*
	rm -rf .diff*

# -------------------- Show warnings

show-warn:			## Print a list of warnings
	@$(ECHO)  "\n\n  *** LIST OF WARNINGS *** \n\n"
	@$(SHOWRN) $(T_DOC).log


# -------------------- SVN update

update:				## Update root path
	$(SVNUP) ./

up: update			## (update)

# -------------------- Clean-up

clean:				## Clean LaTeX files (incl. PDF)
	$(RM) $(addprefix $(T_DOC),$(EXT_CLEAN))
	$(RM) texput.log


# -------------------- Makefile help

help:				## Print help for Makefile list
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^# --------------------)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m %-35s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m # --------------------/[33m===============/'
